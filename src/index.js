// Import of the CSS files
import './css/table.css';
import './css/index.css';

// Import of react components
import React from 'react';
import ReactDOM from 'react-dom';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

// Import of material ui components
import FlatButton from 'material-ui/lib/flat-button';
import Table from 'material-ui/lib/table/table';
import TableHeaderColumn from 'material-ui/lib/table/table-header-column';
import TableRow from 'material-ui/lib/table/table-row';
import TableHeader from 'material-ui/lib/table/table-header';
import TableRowColumn from 'material-ui/lib/table/table-row-column';
import TableBody from 'material-ui/lib/table/table-body';
import FloatingActionButton from 'material-ui/lib/floating-action-button';
import RemoveLawyer from 'material-ui/lib/svg-icons/content/remove';
import $ from 'jquery'; 

// Import of JS files 
import CustomToolbar from './components/CustomToolbar';
import PopoverUpdate from './components/customToolbarUpdate';


var ReadLwayersApi = React.createClass({
    // Function that connect with the API and brings data lawyers.
    loadsLawyersFromApi: function() {
        $.ajax({
            url: this.props.url,
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.setState({data: data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    getInitialState: function() {		
        return {
            data: {
                lawyers: [] 
            }	
        };
    },
    componentDidMount: function() {
        this.loadsLawyersFromApi();
        setInterval(this.loadsLawyersFromApi, this.props.pollInterval);
    },
    // Function that returns the components that will be shown in index.html.
    render: function() {
        return (
            <div className="app">
                <CustomToolbar id="createButton"/><br/>
                <PopoverUpdate id="updateButton"/><br/>
                <h1 className="titleSuits">Suits</h1>
                <ShowLawyers data={this.state.data} />
            </div>
        );
    }
});

var ShowLawyers = React.createClass({
    // Function that connect with the API and delete the lawyer with the select id.
    deleteLawyerApi: function(dataR, indice){
        $.ajax({
            url: "http://dev.nursoft.cl:3000/api/v1/lawyers/"+dataR,
            dataType: 'json',
            type: 'DELETE',
            cache: false,
            success: function(data) {
                alert("Lawyer properly deleted");
                this.deleteLawyer(indice);
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    // Function that receive the index on the table that is selected to delete,
    // and is removed from it.
    deleteLawyer: function(valueLawyerDeleted){
        this.props.data.lawyers.splice( valueLawyerDeleted, 1 );
        this.setState({data: this.props.data.lawyers});
    },
    // Returns the table of the main page.
    render: function() {
        return (
            <div>
                <Table style={{
                    margin: '0 auto'
                    }} >
                    <TableHeader>
                        <TableRow>
                            <TableHeaderColumn >Name</TableHeaderColumn>
                            <TableHeaderColumn >Specialty</TableHeaderColumn>
                            <TableHeaderColumn >Title</TableHeaderColumn>
                            <TableHeaderColumn >Age</TableHeaderColumn>
                            <TableHeaderColumn >Delete</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody>
                        {this.props.data.lawyers.map( (row, index) => (
                            <TableRow id="tdTable" key={index} selected={row.selected}>
                                <TableRowColumn >{row.name}</TableRowColumn>
                                <TableRowColumn>{row.specialty}</TableRowColumn>
                                <TableRowColumn>{row.title}</TableRowColumn>
                                <TableRowColumn>{row.age}</TableRowColumn>
                                <TableRowColumn>
                                    <FloatingActionButton value={index}  
                                        key={index} 
                                        mini={true} 
                                        onTouchTap={this.deleteLawyerApi.bind(this,row.id, index)}>
                                        <RemoveLawyer />
                                    </FloatingActionButton>
                                </TableRowColumn>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </div>
        );
    }
});

ReactDOM.render(<ReadLwayersApi url="http://dev.nursoft.cl:3000/api/v1/lawyers" pollInterval={10000}/>, document.getElementById('app'));
