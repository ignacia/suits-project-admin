import React from 'react';
import Popover from 'material-ui/lib/popover/popover';
import RaisedButton from 'material-ui/lib/raised-button';
import TextField from 'material-ui/lib/text-field';
import FlatButton from 'material-ui/lib/flat-button';
import $ from 'jquery'; 

var Tabletest = React.createClass({
    getInitialState: function() {
        return {
            nameFieldForm: '',
            specialityFieldForm: '',
            titleFieldForm: '',
            ageFieldForm: ''
        };
    },
    // Functions textFieldChangeName, textFieldChangeSpeciality, 
    // textFieldChangeTitle and textFieldChangeAge 
    // receive the current value from text field component
    // and set the new value on the corresponding state variable.
    textFieldChangeName: function(e) {
        this.setState({
            nameFieldForm: e.target.value
        });
    },
    textFieldChangeSpeciality: function(e) {
        this.setState({
            specialityFieldForm: e.target.value
        });
    },
    textFieldChangeTitle: function(e) {
        this.setState({
            titleFieldForm: e.target.value
        });
    },
    textFieldChangeAge: function(e) {
        this.setState({
            ageFieldForm: e.target.value
        });
    },
    // This function receive the values from state variables
    // and call an AJAX function to create the new lawyer.
    createLawyerApi: function(){
        var nameLawyer = this.state.nameFieldForm;
        var specialityLawyer = this.state.specialityFieldForm;
        var titleLawyer = this.state.titleFieldForm;
        var ageLawyer = this.state.ageFieldForm;

        var params = '?lawyer[name]='+nameLawyer+
            '&lawyer[specialty]='+specialityLawyer+
            '&lawyer[title]='+titleLawyer+
            '&lawyer[age]='+ageLawyer;

        $.ajax({    
            url: "http://dev.nursoft.cl:3000/api/v1/lawyers"+params,
            dataType: 'json',
            type: 'POST',
            cache: false,
            success: function(data) {
                alert("Lawyer properly created");
                this.setState({data: data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    // Function that returns the form to add a new lawyer.
    render: function() {
        return (
            <div >
                <TextField 
                    floatingLabelText="Name" 
                    errorText="This field is required." 
                    id="nameForm"
                    value={this.state.nameFieldForm} 
                    onChange={this.textFieldChangeName} /> <br />
                <TextField 
                    floatingLabelText="Speciality" 
                    errorText="This field is required." 
                    id="specialityForm"
                    value={this.state.specialityFieldForm} 
                    onChange={this.textFieldChangeSpeciality} /><br />
                <TextField 
                    floatingLabelText="Title" 
                    errorText="This field is required." 
                    id="titleForm"
                    value={this.state.titleFieldForm} 
                    onChange={this.textFieldChangeTitle} /><br />
                <TextField 
                    floatingLabelText="Age" 
                    errorText="This field is required." 
                    id="ageForm"
                    value={this.state.ageFieldForm} 
                    onChange={this.textFieldChangeAge} /><br />
                <FlatButton 
                    onTouchTap={this.createLawyerApi.bind(null,this)}
                    primary={true} 
                    label="Create"/>
            </div>
        );
    }
});

// This material-ui component is used to display the popover
// used to create a new lawyer.
export default class PopoverExampleSimple extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
        };
    }

    handleTouchTap(event){
        this.setState({
            open: true,
            anchorEl: event.currentTarget,
        });
    }

    handleRequestClose(){
        this.setState({
            open: false,
        });
    }

    render() {
        return (
            <div>
                <RaisedButton
                    onTouchTap={this.handleTouchTap.bind(this)}
                    label="Create"/>
                <Popover
                    open={this.state.open}
                    anchorEl={this.state.anchorEl}
                    anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                    targetOrigin={{horizontal: 'left', vertical: 'top'}}
                    onRequestClose={this.handleRequestClose.bind(this)}>
                    <div>
                        <Tabletest />
                    </div>
                </Popover>
            </div>
        );
    }
}