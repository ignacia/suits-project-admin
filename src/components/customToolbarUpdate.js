import React from 'react';
import Popover from 'material-ui/lib/popover/popover';
import RaisedButton from 'material-ui/lib/raised-button';
import TextField from 'material-ui/lib/text-field';
import SelectField from 'material-ui/lib/select-field';
import MenuItem from 'material-ui/lib/menus/menu-item';

import FlatButton from 'material-ui/lib/flat-button';
import $ from 'jquery';

var FormUpdate = React.createClass({
    getInitialState: function() {
        return {
            nameFieldForm: '',
            specialityFieldForm: '',
            titleFieldForm: '',
            ageFieldForm: '',
            lawyerToUpdate:{
                lawyer:[]
            }
        };
    },  
    componentDidMount: function() {
        this.loadsLawyersUpdateApi();
    },
    // Function that connect with the API and brings the data lawyer selected.
    // Also, save the name, specialty, title and age from the lawyer
    // to the state variables.
    loadsLawyersUpdateApi: function() {
        $.ajax({
            url: 'http://dev.nursoft.cl:3000/api/v1/lawyers/'+this.props.data,
            dataType: 'json',
            cache: false,
            success: function(lawyerUpdate) {
                this.setState({data: lawyerUpdate});
                this.setState({nameFieldForm: lawyerUpdate.lawyer.name});
                this.setState({specialityFieldForm: lawyerUpdate.lawyer.specialty});
                this.setState({titleFieldForm: lawyerUpdate.lawyer.title});
                this.setState({ageFieldForm: lawyerUpdate.lawyer.age});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    // Functions textFieldChangeName, textFieldChangeSpeciality, 
    // textFieldChangeTitle and textFieldChangeAge 
    // receive the current value from text field component
    // and set the new value on the corresponding state variable.
    textFieldChangeName: function(e) {
        this.setState({
            nameFieldForm: e.target.value
        });
    },
    textFieldChangeSpeciality: function(e) {
        this.setState({
            specialityFieldForm: e.target.value
        });
    },
    textFieldChangeTitle: function(e) {
        this.setState({
            titleFieldForm: e.target.value
        });
    },
    textFieldChangeAge: function(e) {
        this.setState({
            ageFieldForm: e.target.value
        });
    },
    // This function receive the values from state variables
    // and call an AJAX function to create the new lawyer.
    updateLawyerApi: function(){
        var nameLawyer = this.state.nameFieldForm;
        var specialityLawyer = this.state.specialityFieldForm;
        var titleLawyer = this.state.titleFieldForm;
        var ageLawyer = this.state.ageFieldForm;

        var params = '?lawyer[name]='+nameLawyer+
            '&lawyer[specialty]='+specialityLawyer+
            '&lawyer[title]='+titleLawyer+
            '&lawyer[age]='+ageLawyer;

        $.ajax({
            url: "http://dev.nursoft.cl:3000/api/v1/lawyers/"
            +this.props.data+params,
            dataType: 'json',
            type: 'PUT',
            cache: false,
            success: function(data) {
                alert("Lawyer properly updated");
                this.setState({data: data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error(this.props.url, status, err.toString());
            }.bind(this)
        });
    },
    // Function that returns the form to update a new lawyer.
    render: function(){
        return (
            <div>
                <TextField 
                    floatingLabelText="Name" 
                    errorText="This field is required." 
                    id="nameForm"
                    value={this.state.nameFieldForm} 
                    onChange={this.textFieldChangeName} /> <br />
                <TextField 
                    floatingLabelText="Specialty" 
                    errorText="This field is required." 
                    id="specialityForm"
                    value={this.state.specialityFieldForm} 
                    onChange={this.textFieldChangeSpeciality} /><br />
                <TextField 
                    floatingLabelText="Title" 
                    errorText="This field is required." 
                    id="titleForm"
                    value={this.state.titleFieldForm} 
                    onChange={this.textFieldChangeTitle} /><br />
                <TextField 
                    floatingLabelText="Age" 
                    errorText="This field is required." 
                    id="ageForm"
                    value={this.state.ageFieldForm} 
                    onChange={this.textFieldChangeAge} /><br />
                <FlatButton 
                    onTouchTap={this.updateLawyerApi.bind(null,this)}
                    primary={true} 
                    label="Update"/>
            </div>
        );
    }
});

var UpdateLawyers = React.createClass({
    // This function change the state of the variable that contains the select
    // id for update.
    chooseLawyerUpd: function(event, index, value){
        this.setState({showForm: true});
        this.setState({value : value});
        this.setState({idLawyerSelect: value});
    },
    // Function that connect with the API and brings data lawyers.
    loadsLawyersFromApi: function() {
        $.ajax({
            url: 'http://dev.nursoft.cl:3000/api/v1/lawyers',
            dataType: 'json',
            cache: false,
            success: function(data) {
                this.setState({lawyersApi: data});
            }.bind(this),
            error: function(xhr, status, err) {
                console.error('http://dev.nursoft.cl:3000/api/v1/lawyers', status, err.toString());
            }.bind(this)
        });
    },
    getInitialState: function() {
        return {
            lawyersApi: {
                lawyers: [] 
            },
            showForm:false,
            idLawyerSelect:{
                id:[]
            }
        };
    },
    componentDidMount: function() {
        this.loadsLawyersFromApi();
    },
    // Function that returns the material-ui component to select the lawyer.
    render: function() {
        return (
            <div>
                <SelectField  
                    value={this.state.value} 
                    onChange={this.chooseLawyerUpd} 
                    floatingLabelText="Select a lawyer">
                    {this.state.lawyersApi.lawyers.map( (row, index) => (
                        <MenuItem 
                            key={index} 
                            value={row.id} 
                            primaryText={row.name}/>
                    ))}
                </SelectField>
                { this.state.showForm ? <FormUpdate data={this.state.idLawyerSelect}/> : null }
                <br />
            </div>
        );
    }
});

// This material-ui component is used to display the popover
// used to update a new lawyer.
export default class PopoverUpdate extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            open: false,
        };
    }

    handleTouchTap(event){
        this.setState({
            open: true,
            anchorEl: event.currentTarget,
        });
    }

    handleRequestClose(){
        this.setState({
            open: false,
        });
    }

    render() {
        return (
            <div>
                <RaisedButton
                    onTouchTap={this.handleTouchTap.bind(this)}
                    label="Update"/>
                <Popover
                    open={this.state.open}
                    anchorEl={this.state.anchorEl}
                    anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                    targetOrigin={{horizontal: 'left', vertical: 'top'}}
                    onRequestClose={this.handleRequestClose.bind(this)}>
                    <div>
                        <UpdateLawyers/>
                    </div>
                </Popover>
            </div>
        );
    }
}
